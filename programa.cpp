#include <iostream>
#include <stdio.h>
#include "Container.h"
#include "Pila.h"

using namespace std;

void op_1(Pila pila[], int cant_pilas){
	int pila_selec;
	string num_serie;
	string nom_empresa;

	cout << "\n\tAGREGAR CONTAINER A LA PILA\n" << endl;
	/* seleccionar columna (pila) con la que se trabajará */
	cout << "Ingrese el numero de la pila: ";
	cin >> pila_selec;
	//int pila_selec = pila_selec - 1;

	/* informacion del container: serie y empresa */
	cout << "\n\tREGISTRO DE CONTAINER\n" << endl;
	cout << "Ingrese serie del container: ";
	cin >> num_serie; //seroe
	cout << "Ingrese empresa encargada del container: ";
	cin >> nom_empresa; //empresa
	
	Container container = Container();
	container.set_serie(num_serie);
	container.set_empresa(nom_empresa);

	pila[pila_selec].Push(container); // restar 1 porq inicia en 0
			
}

void op_2(Pila pila[], int cant_pilas, int limite){
	int pila_selec;
	string num_serie;
	string nom_empresa;

	cout << "\n\tELIMINAR CONTAINER DE LA PILA\n" << endl;
	cout << "\t[BUSQUEDA]" << endl;
	cout << "Ingrese el numero de la pila: ";
	cin >> pila_selec;
	//int pila_selec = pila_selec - 1;
	cout << "Ingrese serie del container: ";
	cin >> num_serie; //seroe
	cout << "Ingrese empresa encargada del container: ";
	cin >> nom_empresa; //empresa
	/* largo valido y 
	if ( (pila_selec <= cant_pilas) && (pila[pila_selec].))
	{
		
	}*/
	while(true){
		Container container = pila[pila_selec].Pop(); //entrar en la pila
		if (container.get_empresa().compare(nom_empresa))
		{
			/* code */
			if (container.get_serie() == num_serie)
			{
				/* coincide la serie */
				diagramaPuerto(pila, cant_pilas, limite);
				break;
			}
		}
		else{
			for (int i = 0; i < cant_pilas; ++i)
			{
				/* code */
				if (i != pila_selec)
				{
					/* code */
					pila[i].Pila_llena();
					if (pila[i].get_band() == false)
					{
						/* code */
						pila[i].Push(container);
						diagramaPuerto(pila, cant_pilas, max);
						break;
					}
				}							
			}
		}
	}

}

void diagramaPuerto(Pila pila[], int cant_pilas, int limite){
	cout << "\n\tDIAGRAMA PUERTO SECO\n" << endl;

	for (int i = (limite);i>=0; i--){
    	for(int j=0; j<cant_pilas; j++){
      		pila[j].Show(i);
    	}
	    cout << endl; //separa niveles (limite)
  	}

  	for(int i=0; i < cant_pilas; i++){
    	cout << "-----------------------"<<endl;
    	cout << "  Columna " << i+1 << "  "<<endl;
    	cout << "-----------------------";
    }

	cout << endl;
}

void menu(Pila pila, int cant_pilas, int limite){
	int opcion;
	
	cout << "\n\t\tMENU\n" << endl;
	cout << " [1] Agregar Container / Push" << endl;
	cout << " [2] Remover Container / Pop" << endl;
	cout << " [3] Ver Puerto / Show" << endl;
	cout << " [4] Salir / Exit" << endl;
	
	cout << "\n Ingrese su opción: ";
	cin >> opcion;

	while(getchar() != '\n');

	switch(opcion){
		case 1:
			diagramaPuerto(pila, cant_pilas, limite);
			
			op_1(pila, cant_pilas);
			
			menu(pila, cant_pilas, limite); //volver a menu
			break;

		case 2:
			op_2(pila, cant_pilas, limite);
			menu(pila, cant_pilas, limite);
			break;

		case 3:
			diagramaPuerto(pila, cant_pilas, limite);
			menu(pila, cant_pilas, limite);
			break;

		case 4:
			exit(1);
			break;

		default:
			cout << "Opcion no valida" << endl;
			break;
	}

}

int main(){
	int cant_pilas; //cantidad pilas en el puerto
	int limite; //tamaño pila

	//Container container = Container();

	cout << "\n\t\tBIENVENIDO\n" << endl;
	cout << "Ingrese la cantidad de pilas: " << endl;
	cin >> cant_pilas;
	/* inicia N pilas */
	Pila *pila = new Pila[cant_pilas];
	
	cout << "Ingrese la cantidad maxima de containers por pila: " << endl;
	cin >> limite;

	for (int i = 0; i < cant_pilas; ++i)
	{
		/* pasar el largo a las N pilas */
		pila[i].Pila_armar(limite);
	}
	
	while(true){
		/* mostrar Menu */
		menu(pila, cant_pilas, limite);
	}
	
	return 0;
}
