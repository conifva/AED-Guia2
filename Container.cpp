#include <iostream>
#include "Container.h"

using namespace std;

/*Constructor*/
Container::Container(){
	this->empresa = "DISPONIBLE";
}

/*Metodos*/
string Container::get_serie(){
    return this->serie;
}

string Container::get_empresa(){
    return this->empresa;
}

void Container::set_serie(string serie){
    this->serie = serie;
}

void Container::set_empresa(string empresa){
	this->empresa = empresa;
}