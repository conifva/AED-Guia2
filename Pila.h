#include <iostream>
#include "Container.h"
using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila{
	private:
		int tope = 0;
		int limite;
		bool band;

		Container *arreglo = NULL;

	public:
		/* constructores */ 
		Pila();
		void Pila_armar(int limite);

		/* métodos get and set */
		int get_limite();
		bool get_band();

		void set_limite(int limite);
		void Pila_vacia();
		void Pila_llena();

		/* opciones menu */
		void Push(Container container);
		Container Pop();
		void Show(int x); //muestra 1 pila

};
#endif
