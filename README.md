# AED-Guia2
Actividades correspondientes a la guia 2 de Algortimo y Estructura de Datos.

# Guia2-UI
Tema: Pilas con arreglos

En un puerto seco se guarda mercaderı́a en contenedores. No es posible colocar más de n contenedores uno encima del otro y no hay área para más de m pilas de contenedores (n y m pueden ser distintos). Cada contenedor tiene un número y el nombre de la empresa propietaria.

Escriba un programa que permita gestionar el ingreso y salida de contenedores. 

	- Para retirar un contenedor es necesario retirar los contenedores que están encima de él y colocarlos en otra pila.
	- Lea por terminal los valores de n y m como parámetros de entrada de su programa.
	- La distribución de los contenedores al ingresar puede ser manual o automática.
	- Muestre por pantalla cada uno de los movimientos que se realizan en el retiro de un contenedor. 

Por ejemplo, para n=4 y m=4, se necesita retirar el contenedor 1/EMP1. La siguiente imagen muestra una posible secuencia de movimientos.

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta clonada y ejecutar por terminal:
```
g++ programa.cpp Container.cpp Pila.cpp -o programa
make
./programa
```

# Acerca de

Programa para manipular y mostrar una pila con arreglos.

El programa solicita inicialmente que el usuario ingrese la cantidad y el tamaño máximo de las pilas que hay en el puerto, luego se presentará un menú con 4 opciones. 

La primera consiste en agregar contenedores al puerto, en caso de seleccionarla se pedira que ingrese los datos de identificacion del conteiner (numero de serie y empresa responsable). La segunda opción permite eliminar contenedores anteriormente ingresados, solo se permite eliminar UN contenedor a la vez. En opción tres se encarga de mostrar el contenido del puerto. Finalmente la cuarta opción le permite al usuario cerrar el programa.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela
