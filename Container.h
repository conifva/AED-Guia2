#include <iostream>

using namespace std;

#ifndef CONTAINER_H
#define CONTAINER_H

class Container{
    private:
        string serie;
        string empresa;
    
    public:
    	/*Constructor*/
    	Container();
    	
    	/*Metodos*/
        string get_serie();
        string get_empresa();

        void set_serie(string serie);
        void set_empresa(string empresa);
};
#endif
